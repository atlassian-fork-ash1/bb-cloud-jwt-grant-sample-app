
Bitbucket Cloud JWT Grant Sample App
---

## Introduction
This app aims to demonstrate the JWT Grant Type Authorization flow, what is needed to generate an access_token and how to use that to invoke Bitbucket REST APIs, and lastly a reference app where the developer can copy paste the code and try it in their own projects

## What is JWT Grant?
Custom grant type developed by Atlassian for it's Connect apps.

If your Atlassian Connect app uses JWT authentication, you can swap a JWT for an OAuth access token. The resulting access token represents the account for which the app is installed.

Make sure you send the JWT token in the Authorization request header using the “JWT” scheme (case sensitive). Note that this custom scheme makes this different from HTTP Basic Auth (and so you cannot use “curl -u”).

This is a little tricky because at first you might think that you need the consumerKeys and consumerSecrets you generated thru Bitbucket Settings, but actually the consumer of the API is the app. That being said:

* client_id = app clientId from /installed lifecycle
* client_secret = app sharedSecret from /installed lifecycle

### Difference with other grants
- Don't need user initiation
- Can be used directly to obtain an access_token
- Custom grant type
- For Connect apps wanting to use oauth and swapped with JWT
- Generating the bearer token would need a jwt library to encode/decode

### Authorization Flow Chart
![Alt text](public/images/authorization_flow.png?raw=true "Bitbucket Authorization Grant Flow")

### Things to consider
*What is OAuth?*

OAuth is a mechanism for authorizing an entity and granting them access over which the user is capable of doing in an application

*What it is not?*

OAuth is not a way to authenticate but rather to authorize an entity for access

*Definition of terms*

| Term          									| Description   |
|---------------------------------------------------|-------------|
|Basic Auth    										|Is an authentication mechanism. Is straightforward but unsecure. It is the combination of your username and password that you would use to access an API endpoint.|
|client_id     										|Key generated from Bitbucket Settings → Add Consumer   |
|secret        										|Secret generated from Bitbucket Settings → Add Consumer|
|https://bitbucket.org/site/oauth2/authorize        |URL that you use to authorize an entity|
|https://bitbucket.org/site/oauth2/access_token		|URL that generates an access token for an entity|
|access_token										|Defines the permission of the selected user, ergo represents the authorization of a specific application to access specific parts of a user's data. Tokens that represents the authorization of an entity to access specific parts of a user's data in an application. It is the token generated or returned by https://bitbucket.org/site/oauth2/access_token|

*Participants*

| Role          									| Description   |
|---------------------------------------------------|-------------|
|Resource Owner/End-user    						|An entity capable of granting access to a protected resource. When the resource owner is a person, it is referred to as an end- user.|
|Client     										|An application making protected resource requests on behalf of the resource owner and with its authorization.|
|Resource Server        							|The server hosting the protected resources, capable of accepting and responding to protected resource requests using access tokens.|
|Authorization Server						       	|The server issuing access tokens to the client after successfully authenticating the resource owner and obtaining authorization.|

## Prerequisite
1. NodeJs

## Running the app
1. In the project's root directory, run `npm install`. This should download all the dependencies for you needed by the app
1. Once download is complete, run ngrok: `ngrok http 3000` 
1. Once ngrok is running, copy the ngrok url and update the baseUrl of your app in atlassian-connect.json
1. Run `npm start` and if the app is running by going to http://localhost:3000/healthcheck, this should display an 'OK' response
1. Have a consistent sharedSecret
    1. Make sure that you are not using the same key as this app, if you are - modify the app key of this app in atlassian-connect.json
    1. Go to your Bitbucket Settings ![Alt text](public/images/DevelopApps_0.png?raw=true "Bitbucket Settings")
    1. Develop Apps ![Alt text](public/images/DevelopApps_1.png?raw=true "Develop Apps")
    1. Click on Create App ![Alt text](public/images/DevelopApps_2.png?raw=true "Create App")
    1. Paste your ngrok url as such: https://{subdomain}.ngrok.io/atlassian-connect.json ![Alt text](public/images/DevelopApps_3.png?raw=true "Create App with ngrok URL")
    1. You now have an app with a consistent share secret you can redeploy later ![Alt text](public/images/DevelopApps_4.png?raw=true "Your new app")
1. While inside the Bitbucket Settings, install the app
    1. Click Installed Apps ![Alt text](public/images/InstallApps_0.png?raw=true "Install Apps")
    1. Click on "Install from URL" ![Alt text](public/images/InstallApps_1.png?raw=true "Install from URL")
    1. Paste https://{subdomain}.ngrok.io/atlassian-connect.json and click "Install" ![Alt text](public/images/InstallApps_2.png?raw=true "Install from URL")
    1. Grant the app the necessary permission ![Alt text](public/images/InstallApps_3.png?raw=true "Install Apps")
    1. You need to refresh the page for your app to reflect ![Alt text](public/images/InstallApps_4.png?raw=true "Install Apps")
    1. Your app is now installed ![Alt text](public/images/InstallApps_5.png?raw=true "Install Apps")
1. Go to any respository, and click on 'JWT Authorization Grant' in the sidebar. This will initiate the grant flow

## Making changes to the app
For any front-end changes, you don't need to reinstall the app. 

However, if there's change in the back-end or descriptor, a restart of the server is needed which will erase all the in memory database, including the connection id and sharedSecret which are needed by Bitbucket to authenticate your app. 

Do the following to deploy and reinstall your app and maintain sharedSecret:

1. If you updated your descriptor, go to Bitbucket Settings -> Develop Apps -> Update . This will refresh your app descriptor
1. For any back-end changes that will result in a server restart and erase in memory database, do the follow:
	1. Copy the ngrok {subdomain} in your descriptor (https://{subdomain}.ngrok.io)
	1. Run `ngrok http -subdomain={subdomain} 3000`
	1. Run npm start
	1. Go to Bitbucket Settings -> Install App from URL and paste https://{subdomain}.ngrok.io/atlassian-connect.json, this will ask you to grant the app the necessary permission
	1. Go to any respository, and click on 'JWT Authorization Grant' in the sidebar. This will initiate the grant flow

## Navigating the app

| Endpoint 			| Description |
|-------------------|-------------|
| /					|Redirects the user to the landing page of the sample app |
| /healthcheck		|Test whether your app is alive							|
| /oauth-callback	|Initiate get access_token|

## Support

If you have any questions about this app or simply want to give us feedback please do so in the [Bitbucket Cloud category](https://community.developer.atlassian.com/c/bitbucket-development/bitbucket-cloud) on the Developer Community.

## Related Documentation
- [Bitbucket OAuth 2.0](https://developer.atlassian.com/cloud/bitbucket/oauth-2/)
- [OAuth Participants](https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-1.1)
