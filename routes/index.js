var util = require('util');
var jwt = require('atlassian-jwt');
var moment = require('moment');
var config = require('../config');

var request = require("request");

module.exports = function (app, addon) {
    //JSON Object for our presentation in html
    var auth_grant = {
        oauth_link: "https://bitbucket.org/account/user/bitbucket-cloud-dev/api",
        response_type: "code",
        type: {
            auth_grant_type : "Bitbucket Cloud JWT Grant",
            definition: "<p>" +
                            "Custom grant type developed by Atlassian for it's Connect apps." +
                        "</p>" +
                        "<p>" +
                            "If your Atlassian Connect app uses JWT authentication, you can swap a JWT for an OAuth access token. The resulting access token represents the account for which the app is installed." +
                        "</p>" +
                        "<p>" +
                            "Make sure you send the JWT token in the Authorization request header using the “JWT” scheme (case sensitive). Note that this custom scheme makes this different from HTTP Basic Auth (and so you cannot use “curl -u”)." +
                        "</p>" +
                        "<p>" +
                            "This is a little tricky because at first you might think that you need the consumerKeys and consumerSecrets you generated thru Bitbucket Settings, but actually the consumer of the API is the app. That being said:" +
                            "<ul>" +
                                "<li>client_id = app clientId from /installed lifecycle</li>" +
                                "<li>client_secret = app sharedSecret from /installed lifecycle</li>" +
                            "</ul>" +
                        "</p>",
            diff_with_other_grants: [
                "Don't need user initiation",
                "Can be used directly to obtain an access_token",
                "Custom grant type",
                "For Connect apps wanting to use oauth and swapped with JWT",
                "Generating the bearer token would need a jwt library to encode/decode",
            ],
            document_link : "https://developer.atlassian.com/cloud/bitbucket/oauth-2/"
        },
        client: {
            key: "",
            secret: process.env.my_app_share_secret,
            jwt_bearer: ""
        },
        access_token: {
            access_token: "",
            request: {},
            response: {}
        },
        repository: {
            request: {},
            response: {}
        }
    };

    //healthcheck route used by micros to ensure the addon is running.
    app.get('/healthcheck', function(req, res) {
        res.send(200);
    });

    // Root route. This route will redirect to the add-on descriptor: `atlassian-connect.json`.
    app.get('/', function (req, res) {
        res.format({
            // If the request content-type is text-html, it will decide which to serve up
            'text/html': function () {
                res.redirect('/atlassian-connect.json');
            },
            // This logic is here to make sure that the `atlassian-connect.json` is always
            // served up when requested by the host
            'application/json': function () {
                res.redirect('/atlassian-connect.json');
            }
        });
    });

    app.post('/oauth-callback', function(req, res) {
        console.log('callback received');
    });

    /*
     * This is where the action happens! After the user authorizes the app, this endpoint processes the access_token
     * and invokes a REST API
     */
    app.get('/oauth-callback', addon.authenticate(), function (req, res) {
        console.log('==================================================================');
        console.log('Start building our JWT token to be used an Authorization bearer to get an access_token');
        console.log('==================================================================');

        let unverifiedClaims = '';
        try {
            unverifiedClaims = jwt.decode(req.query.jwt, '', true); // decode without verification;
            console.log(`JWT Claims: \n ${JSON.stringify(unverifiedClaims, null, 2)}`);
        } catch (e) {
            console.log(e);
        }

        let issuer = unverifiedClaims.iss;          // connection id from /installed
        console.log(`Connection ID from /installed lifecycle: ${issuer}`);

        const now = moment().utc();
        const tokenData = {
            "iss": addon.key,
            "iat": now.unix(),                      // the time the token is generated
            "exp": now.add(3, 'minutes').unix(),    // token expiry time (recommend 3 minutes after issuing)
            "sub": issuer                           // connection id from /installed
        };

        const secret = process.env.my_app_share_secret; // app sharedSecred from /installed

        console.log(`Generate the JWT bearer using HS256 algorithm with the following parameters: \n ${JSON.stringify(tokenData, null, 2)} \n and sharedSecret: ${secret}`)
        const token = jwt.encode(tokenData, secret, 'HS256');

        console.log('Update the auth_grant object for our view presentation');
        auth_grant.client.key = issuer;
        auth_grant.client.secret = secret;
        auth_grant.client.jwt_bearer = token;

        console.log('==================================================================');
        console.log('Start building our request to get an access_token');
        console.log('==================================================================');

        let access_token_options = { method: 'POST',
            url: 'https://bitbucket.org/site/oauth2/access_token',
            headers:
                { 'Content-Type': 'application/x-www-form-urlencoded',
                    Authorization: `JWT ${token}`
                    },
            form: { grant_type: 'urn:bitbucket:oauth2:jwt' }
        };

        console.log(`Request \n ${JSON.stringify(access_token_options, null, 2)}`);
        request(access_token_options, function (error, response, body) {
            if (error) throw new Error(error);

            let body_json = JSON.parse(body);
            let access_token = body_json.access_token;

            console.log(`Response: \n ${JSON.stringify(body_json, null, 2)}`);
            console.log('==================================================================');
            console.log(`Callback from authorization server with access_token: ${access_token} to be used for API requests`);
            console.log('==================================================================');
            console.log(`Start building our request to GET end-user's repository with access_token : ${access_token}`);
            console.log('==================================================================');

            auth_grant.access_token.access_token = access_token;
            auth_grant.access_token.request = JSON.stringify(access_token_options, null, 2);
            auth_grant.access_token.response = JSON.stringify(body_json, null, 2);

            let options = { method: 'GET',
                url: 'https://api.bitbucket.org/2.0/repositories',
                headers:
                    {
                        'Cache-Control': 'no-cache',
                        Authorization: `Bearer ${access_token}`
                    }
            };

            console.log(`Request options for API invocation: \n ${JSON.stringify(options, null, 2)}`);
            request(options, function (error, response, body) {
                if (error) throw new Error(error);

                console.log(body);

                auth_grant.repository.request = JSON.stringify(options, null, 2);
                auth_grant.repository.response = JSON.stringify(JSON.parse(body), null, 2);
                res.render('request-access-token', auth_grant);
            });
        });
    });
};
